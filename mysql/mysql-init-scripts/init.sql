DROP DATABASE IF EXISTS appdatabase;
CREATE DATABASE appdatabase;

CREATE USER 'metabase01'@'%' IDENTIFIED WITH mysql_native_password BY 'metabase0110';
GRANT ALL PRIVILEGES ON appdatabase.* TO 'metabase01'@'%';